DECLARE @User nvarchar(MAX); SELECT @User= BulkColumn FROM OPENROWSET (BULK '/var/opt/mssql/admin_users.json', SINGLE_CLOB) as j;
INSERT INTO repassist.dbo.admin_users(mongo_id, username, password,is_active, salt) SELECT * FROM OPENJSON(@User)  WITH (
mongo_id varchar(200) '$._id."$oid"',
username varchar(200) '$.username',
password varchar(200) '$.password',
is_active bit '$.is.active',
salt varchar(200) '$.salt'
)
GO

DECLARE @User nvarchar(MAX); SELECT @User= BulkColumn FROM OPENROWSET (BULK '/var/opt/mssql/database_versions.json', SINGLE_CLOB) as j;
INSERT INTO repassist.dbo.database_versions(mongo_id,app,needs_update,version_timestamp) SELECT * FROM OPENJSON(@User)  WITH (
mongo_id varchar(255) '$._id."$oid"',
app varchar(255) '$.app',
needs_update bit '$.needs_update',
version_timestamp bigint '$.version_timestamp'
)
GO

DECLARE @User nvarchar(MAX); SELECT @User= BulkColumn FROM OPENROWSET (BULK '/var/opt/mssql/ampcolists.json', SINGLE_CLOB) as j;
INSERT INTO repassist.dbo.ampcolists(mongo_id, author, name, file1, list, created) SELECT * FROM OPENJSON(@User)  WITH
(
 mongo_id varchar(200) '$._id."$oid"',
      author varchar(200) '$.author',
name varchar(200) '$.name',
file1 varchar(200) '$.file',
list varchar(200) '$.list',
created datetime '$.created."$date"'
 )
GO