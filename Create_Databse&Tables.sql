USE [master]
GO

CREATE DATABASE [repassist]
GO

USE [repassist]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[admin_users](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[mongo_id] [varchar](200) NULL,
	[username] [varchar](200) NULL,
	[is_active] [bit] NULL,
	[password] [varchar](200) NULL,
	[salt] [varchar](200) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ampco_customers](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[customer_id] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[initials] [varchar](255) NULL,
	[surname] [varchar](255) NULL,
	[address_line1] [varchar](255) NULL,
	[address_line2] [varchar](255) NULL,
	[address_line3] [varchar](255) NULL,
	[address_line4] [varchar](255) NULL,
	[suburb] [varchar](255) NULL,
	[state] [varchar](255) NULL,
	[postcode] [varchar](255) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ampco_pharmacies](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[pharmacy_id] [int] NULL,
	[name] [varchar](255) NULL,
	[address_line1] [varchar](255) NULL,
	[address_line2] [varchar](255) NULL,
	[address_line3] [varchar](255) NULL,
	[address_line4] [varchar](255) NULL,
	[suburb] [varchar](255) NULL,
	[state] [varchar](255) NULL,
	[postcode] [varchar](255) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[ampcolists](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[mongo_id] [varchar](200) NULL,
	[author] [varchar](200) NULL,
	[name] [varchar](200) NULL,
	[file1] [varchar](200) NULL,
	[list] [varchar](200) NULL,
	[created] [datetimeoffset](7) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[database_versions](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[mongo_id] [varchar](255) NULL,
	[app] [varchar](225) NOT NULL,
	[needs_update] [bit] NULL,
	[version_timestamp] [bigint] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[database_versions]  WITH CHECK ADD CHECK  (([app]='sample_images' OR [app]='sample' OR [app]='accc'))
GO

CREATE TABLE [dbo].[samples](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[sku] [varchar](255) NULL,
	[category] [varchar](255) NULL,
	[description] [varchar](255) NULL,
	[quantities] [varchar](255) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Sessions](
	[session_id] [nvarchar](32) NOT NULL,
	[expires] [datetimeoffset](7) NULL,
	[data] [nvarchar](max) NULL,
	[createdAt] [datetimeoffset](7) NOT NULL,
	[updatedAt] [datetimeoffset](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[session_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

